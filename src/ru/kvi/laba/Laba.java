package ru.kvi.laba;

import java.util.Scanner;

public class Laba {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        double y, x, a, z, f;

        System.out.print("Введите x : ");
        x = scanner.nextDouble();

        System.out.print("Ведите a : ");
        a = scanner.nextDouble();

        z = 3 - (x / a);
        f = 2 - ((x - a) / z);

        if (a != 0 && z != 0 && f != 0) {

            System.out.printf("y = %.2f", ((1 - x + a) / f));
        } else {
            System.out.println("На ноль делить нельзя");
        }
    }
}
